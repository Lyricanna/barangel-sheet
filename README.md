# Alternate character sheet for Pathfinder 1e

This is a alternative character sheet for the Pathfinder 1e system on FoundryVTT.

It is a combination of the 3.5 SRD sheet (the old PF1 sheet) and the current PF1 sheet with some custom modifications.

This sheet might not have features that the default sheet has.
If you think something essential is missing, please submit an issue here on GitLab.

## Manifest

Manifest for Foundry installation: https://gitlab.com/zenvy/foundryvtt-pf1-alt-sheet/-/raw/master/module.json

## Known Issues

- None

## Links

[Pathfinder 1e system](https://gitlab.com/Furyspark/foundryvtt-pathfinder1)

[3.5 SRD system](https://github.com/Rughalt/D35E)

## Pics

![First page](img/front.png)

![Attributes](img/attributes.png)

![Combat](img/combat.png)

![Features](img/features.png)

![Skills](img/skills.png)

![Spells](img/spells.png)
