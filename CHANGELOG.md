# 0.7.6

- Fix pf1 0.80.15 compat

# 0.7.5

- Fix item tags in inventory not showing when hovering over the resources
- Show use icon for prepared spells that have been used
- Fix compat with pf1 v0.80.10
- Fix alt chat card button widths

# 0.7.4

- Fix checks in skillAlwaysShown

# 0.7.3

- Fix arbitrary skill addition and deletion

# 0.7.2

- Fix alternate chat card roll details on click
- Add target display to alt chat card

# 0.7.1

- Fix spellbook toggling with pf1 0.80.7

# 0.7.0

- Fix skill rolling in pf1 0.80+
- Compat with Foundry v9

# 0.6.2

- Fix display of three-digit speeds
- Fix no spell description in alternate chat cards provided by this module
- Show disabled features as disabled

# 0.6.1

- Fix rolling concentration and caster level checks

# 0.6.0

- Required PF1 version update
- Fix CMB display being double the value
- Fix carry capacity input field

# 0.5.2

- Fix nonlethal damage apply in the chat card not working

# 0.5.1

- Improve chat card a bit
- Fixed at-will spells not having a use action button (thanks websterguy)
- Fix CMB display

# 0.5.0

- Added alternative attack chat card style
- Fix some condition compendium links
- Update attack and encumbrance tooltips to match the default sheet

# 0.4.5

- Added shortcut to buff browser on conditions tab (thanks Fair Strides)

# 0.4.4

- Added Spanish translation (thanks Wharomaru Zhamal)

# 0.4.3

- Added some checks to prevent some sheet not rendering issues

# 0.4.2

- Potentially fix setting as default sheet not working
- Add Italian translation (thanks Davide Mascaretti)

# 0.4.1

- Fix feature charges tooltip showing no item tag.
- Show class HP and possible max HP in features tab
- Fix alt sheet config updates not force a sheet render
- Update settings window to match pf1 settings
- Made base skills rollable

# 0.4.0

- Added interaction with [Actor Link](https://gitlab.com/koboldworks/pf1/actor-link).
  - When the sheet is set to Actor Link Mode Familiar, it will make it possible to roll with the ranks of the parent actor on the familiar.
  - Known issue: the actor sheet needs to be opened twice before the master ranks are shown.
  - Known issue: changing the master will not update the familiar, the sheet has to be reopened first.

# 0.3.3

- Fix an error with the AIP integration when AIP was installed but not active
- Fix charged quick actions not displaying charges
- Show warning when too many feats are on an actor
- Show warning when too many skill ranks are used by an actor
- Re-add compendium links for conditions

# 0.3.2

- Fix some layouting with CN locale
- Update AIP API to v2
- Implement item list filtering via search box like default sheet

# v0.3.1

- Use the conditions display/toggle-thingy from the default sheet

# v0.3.0

- Compat with Foundry v0.8.6 and PF1 >v0.78.0
- Moved spellslot progression radio buttons up

# v0.2.4

- Added a tooltip to the caster level in spellbooks to show the roll + bonus
- Autoconvert the speed to metric. This forces you to enter the speed in ft even when using metric.
  - There is a tooltip explaining that you need to do this, but there is no automatic migration.
  - This is done to do the same as the default sheet because the converted value is now provided to Drag Ruler.
- Added a checkbox to the class list to switch the layout to something more compact.
- Use a different CSS class for the instructions in the features tab to improve compatibility with Koboldworks
- Fix charges not being shown on the summary tab.
- Fix compatibility with PF1 0.77.22, especially the new spell slot system.
  - Warning: Some things might be missing/not working yet!
